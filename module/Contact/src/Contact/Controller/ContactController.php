<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Contact\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Contact\Form\ContactForm;

class ContactController extends AbstractActionController
{
    public function addAction()
    {
        $form = new ContactForm();
        $form->get('submit')->setValue('Add');
        //return new ViewModel();
        
        //$request = $this->getRequest();
        //if ($request->isPost()) {
        //}
        
        return array('form' => $form);
    }
}
